pipeline {
    agent any
    
    environment {
        registry = "kss7/dotnetapp"
        img = "${registry}:${env.BUILD_ID}"
        containerName = "webapp_container"
        portMapping = "8081:80" // Change the port mapping as needed
        DOCKER_CREDENTIALS = credentials('ass_02_3956')
    }
    
    stages {
        stage('Checkout') {
            steps {
                git branch: 'main', url: 'https://gitlab.com/H-Neethika/3956-hariharasakthy.git'
            }
        }
        
        stage('Build and Publish') {
            steps {
                script {
                    docker.build("${img}", "-f Dockerfile .")
                }
            }
        }
        
        stage('Stop and Remove Docker Container') {
            steps {
                script {
                    // Stop the container
                    sh 'docker stop webapp_container'
                    
                    // Remove the container
                    sh 'docker rm webapp_container'
                }
            }
        }

        stage('Run Container') {
            steps {
                script {
                    sh "docker run -d --name ${containerName} -p ${portMapping} ${img}"
                }
            }
        }
    }

    post {
        success {
            script {
                // Remove the Docker image
                sh "docker rmi -f ${img}"
            }
        }
        
        failure {
            // Cleanup actions in case of failure
            script {
                // Remove the Docker image
                sh "docker rmi -f ${img}"
            }
        }
    }
}
